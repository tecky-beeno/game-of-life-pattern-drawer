let pattern = `
..........
.xxxxxxxx.
....x.....
....x.....
....x.....
....x.....
..........
`

let lines = pattern.split('\n')
lines.pop()
lines.shift()
lines = lines.map((line, dy) =>
  line.split('').map((char, dx) => {
    let value = char === '.' ? 0 : 1
    return {
      value,
      dx,
      dy,
    }
  }),
)

console.log('>>>>')
console.log(lines)
console.log('<<<<')
